#!/bin/bash

declare -a MEDIA=("bbb_30fps_1024x576_2500k"
                  "bbb_30fps_1280x720_4000k"
                  "bbb_30fps_1920x1080_8000k")

if [[ "$OSTYPE" = darwin* ]]; then
    export BINDIR=$HOME/Bento4-SDK-1-6-0-637.universal-apple-macosx
else
    export BINDIR=$HOME/Bento4-SDK-1-6-0-637.x86_64-unknown-linux
fi

for PREFIX in "${MEDIA[@]}"
do
  $BINDIR/bin/mp42hls \
      --segment-filename-template "${PREFIX}_%d.ts" \
      --segment-url-template "${PREFIX}_%d.ts" \
      --index-filename ${PREFIX}.m3u8 \
      ${PREFIX}.mp4
done
